;;Enable ido
(setq ido-enable-flex-matching t)
  (setq ido-everywhere t)
  (ido-mode 1) 
  (setq inhibit-startup-message t)
 
;;Load custom theme
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes") 
(load-theme 'smyx t)
  
(require 'org)
;; Make org-mode work with files ending in .org
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
;; The above is the default in recent emacsen

;;Define a function to compile and run the actual buffer
(defun run-current-file ()
  "Runs the compilation of the current file.
Assumes it has the same name, but without an extension"
  (interactive)
  (compile (file-name-sans-extension buffer-file-name)))

(require 'iso-transl)

;;Configure Outbound Mail

;;Tell the program who you are

(setq user-full-name "José Alejandro Camiño Iglesias")
(setq user-mail-address "ja.camino@alumnos.upm.es")

;;Tell Emacs to use GNUTLS instead of STARTTLS
;;to authenticate when sending mail.

(setq starttls-use-gnutls t)

;;Tell Emacs about your mail server and credentials

(setq send-mail-function 'smtpmail-send-it
message-send-mail-function 'smtpmail-send-it
smtpmail-starttls-credentials
'(("smtp.upm.com" 587 nil nil))
smtpmail-auth-credentials
(expand-file-name "~/.authinfo")
smtpmail-default-smtp-server "smtp.upm.es"
smtpmail-smtp-server "smtp.upm.es"
smtpmail-smtp-service 587
smtpmail-debug-info t)
(require 'smtpmail)


(require 'compile)
 (add-hook 'c-mode-hook
           (lambda ()
	     (unless (file-exists-p "Makefile")
	       (set (make-local-variable 'compile-command)
                    ;; emulate make's .c.o implicit pattern rule, but with
                    ;; different defaults for the CC, CPPFLAGS, and CFLAGS
                    ;; variables:
                    ;; $(CC) -c -o $@ $(CPPFLAGS) $(CFLAGS) $<
		    (let ((file (file-name-nondirectory buffer-file-name)))
                      (format "%s -o %s %s %s %s"
                              (or (getenv "CC") "gcc")
                              (file-name-sans-extension file)
                              (or (getenv "CPPFLAGS") "")
                              (or (getenv "CFLAGS") "-ansi -pedantic -Wall -Wextra")
			      file))))))

(require 'org-latex)
(unless (boundp 'org-export-latex-classes)
  (setq org-export-latex-classes nil))
(add-to-list 'org-export-latex-classes
             '("article"
               "\\documentclass{article}"
               ("\\section{%s}" . "\\section*{%s}")))


;; Mostrar parentesis
(show-paren-mode 1)
  (require 'paren)
    (set-face-background 'show-paren-match "magenta")
    (set-face-foreground 'show-paren-match "white")
    (set-face-attribute 'show-paren-match nil :weight 'extra-bold)


;;Bindeo movimiento entre parentesis, ahora te mueves con Meta - flecha arriba o fleba abajo entre parentesis. 
(global-set-key (kbd "M-<down>") 'forward-list)
(global-set-key (kbd "M-<up>") 'backward-list)
(global-set-key (kbd "C-M-<down>") 'end-of-buffer);;C-M-flecha abajo --> final buffer
(global-set-key (kbd "C-M-<up>") 'beginning-of-buffer);; C-M-flecha arriba --> inicio buffer


;; Busqueda y copia segun expresion regular
(defun copy-lines-matching-re (re)
  "find all lines matching the regexp RE in the current buffer
putting the matching lines in a buffer named *matching*"
  (interactive "sRegexp to match: ")
  (let ((result-buffer (get-buffer-create "*matching*")))
    (with-current-buffer result-buffer 
      (erase-buffer))
    (save-match-data 
      (save-excursion
        (goto-char (point-min))
        (while (re-search-forward re nil t)
          (princ (buffer-substring-no-properties (line-beginning-position) 
                                                 (line-beginning-position 2))
                 result-buffer))))
    (pop-to-buffer result-buffer)))

(global-set-key (kbd "C-M-W") 'copy-lines-matching-re);; Bindeo para la anterior función

;; Adding ElDoc
;;(add-hook 'emacs-lisp-mode-hook 'turn-on-eldoc-mode)
;;     (add-hook 'lisp-interaction-mode-hook 'turn-on-eldoc-mode)
;;     (add-hook 'ielm-mode-hook 'turn-on-eldoc-mode)

